from django.urls import path
from  . import views


urlpatterns = [
    path('hello/', views.hello_world_text, name='Hello'),
    path('book_programm_lang/', views.book_view, name='book'),
]

