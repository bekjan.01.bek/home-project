# import requests
# from bs4 import BeautifulSoup as bs
# from django.views.decorators.csrf import csrf_exempt
#
# URL = "http://www.ts.kg"
#
# HEADERS = {
#     "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
# }
#
# @csrf_exempt
# def get_html(url, params=None):
#     req = requests.get(url, headers=HEADERS, params=params)
#     req.raise_for_status()
#     return req
#
# @csrf_exempt
# def get_data(html):
#     soup = bs(html, "html.parser")
#     items = soup.find_all("div", class_="app-shows-item-full")
#     ts_film = []
#     for item in items:
#         ts_film.append(
#             {
#                 "title_text": item.find("span", class_="app-shows-card-title").get_text(),
#                 "title_url": URL + item.find("a").get("href"),
#                 "image": URL + item.find("img").get("src"),
#             }
#         )
#     return ts_film
#
# def parser():
#     html = get_html(URL)
#     if html.status_code == 200:
#         ts_film2 = []
#         for page in range(0, 1):
#             html = get_html(f"http://www.ts.kg/category/films", params=page)
#             ts_film2.extend(get_data(html.text))
#             print(ts_film2)
#
#     else:
#         raise Exception("Ошибка parser")
#
# parser()