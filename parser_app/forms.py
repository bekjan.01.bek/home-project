from django import forms
from . import parser, models


class ParserForm(forms.Form):
    MEDIA_CHOICES = (
        ('TS.KG', 'TS.KG'),
    )

    media_type = forms.ChoiceField(choices=MEDIA_CHOICES)

    class Meta:
        fields = ['media_type']

    def parser_data(self):
        if self.data['media_type'] == 'TS.KG':
            film_parser = parser.parser()
            if film_parser is not None:
                for i in film_parser:
                    models.FilmParser.objects.create(**i)
            else:
                print("film_parser is None")

