from django import forms
from django.contrib.auth.forms import UserCreationForm
from . import models

USER_TYPE = (
        ('Админ', 'Админ'),
        ('Супер_Админ', 'Супер_Админ'),
        ('Клиент', 'Клиент'),
        ('VIP-Клиент', 'VIP-Клиент'),
        ('ЧУВАК', 'ЧУВАК')
    )
MARIED = (
    ('Да', 'Да'),
    ('Нет', 'Нет')
)

GENDER = (
        ('Мужской', 'Мужской'),
        ('Женский', 'Женский')
    )
CHILDREN = (
        ('Да', 'Да'),
        ('Нет', 'Нет')
    )
VS = (
        ('СОН', 'СОН'),
        ('ЕДА', 'ЕДА')
    )

DN = (
        ('ДЕНЬ', 'ДЕНЬ'),
        ('НОЧЬ', 'НОЧЬ')
    )

class CustomUserRegistrations(UserCreationForm):
    email = forms.EmailField(required=True)
    user_type = forms.ChoiceField(choices=USER_TYPE, required=True)
    name = forms.CharField(required=True) # Имя
    surname = forms.CharField(required=True)  # Фамилия
    patronymic = forms.CharField(required=True)  # Отчество
    image = forms.ImageField(required=True)  # Фото
    phone_number = forms.CharField(required=True)  # Номер
    maried = forms.ChoiceField(choices=MARIED, required=True)  # Семейное положение
    gender = forms.ChoiceField(choices=GENDER, required=True)  # Пол
    children = forms.ChoiceField(choices=CHILDREN, required=True)  # Дети
    age = forms.IntegerField(required=True)  # Возрост
    nation = forms.CharField(required=True)  # Национальность
    weight = forms.CharField(required=True)  # Религия
    country = forms.CharField(required=True)  # Страна
    city = forms.CharField(required=True)  # Город
    address = forms.CharField(required=True)  # Адресс
    hobby = forms.CharField(required=False)  # Хобби
    language = forms.CharField(required=False)  # Языки
    education = forms.CharField(required=False)  # Оброзование
    profession = forms.CharField(required=False)  # Профессия
    about_me = forms.TextInput()  # О себе
    favorite_vs = forms.ChoiceField(choices=VS, required=False)  # Выбор
    favorite_dn = forms.ChoiceField(choices=DN, required=False) # Выбор
    favorite_film = forms.CharField(max_length=250)  # Любимый фильм


    class Meta:
        model = models.CustomUser
        fields = (
            "username",
            "email",
            "password1",
            "password2",
            "user_type",
            "name",
            "surname",
            "patronymic",
            "image",
            "phone_number",
            "maried",
            "gender",
            "children",
            "age",
            "nation",
            "weight",
            "country",
            "city",
            "address",
            "hobby",
            "language",
            "education",
            "profession",
            "about_me",
            "favorite_vs",
            "favorite_dn",
            "favorite_film",
        )

    def save(self, commit=True):
        user = super(CustomUserRegistrations, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user

#Интерфейс на Русском и да оно работает

# class CustomUserRegistrations(UserCreationForm):
#     email = forms.EmailField(required=True)
#     user_type = forms.ChoiceField(choices=USER_TYPE, required=True)
#     Имя = forms.CharField(required=True) # Имя
#     Фамилия = forms.CharField(required=True)  # Фамилия
#     Отчество = forms.CharField(required=True)  # Отчество
#     Фото = forms.ImageField(required=True)  # Фото
#     Номер_Телефона = forms.CharField(required=True)  # Номер
#     Семейное_Положение = forms.ChoiceField(choices=MARIED, required=True)  # Семейное положение
#     Пол = forms.ChoiceField(choices=GENDER, required=True)  # Пол
#     Дети = forms.ChoiceField(choices=CHILDREN, required=True)  # Дети
#     Возрост = forms.IntegerField(required=True)  # Возрост
#     Национальность = forms.CharField(required=True)  # Национальность
#     Религия = forms.CharField(required=True)  # Религия
#     Страна = forms.CharField(required=True)  # Страна
#     Город = forms.CharField(required=True)  # Город
#     Адресс = forms.CharField(required=True)  # Адресс
#     Хобби = forms.CharField(required=False)  # Хобби
#     Языки = forms.CharField(required=False)  # Языки
#     Оброзование = forms.CharField(required=False)  # Оброзование
#     Профессия = forms.CharField(required=False)  # Профессия
#     about_me = forms.TextInput()  # О себе
#     Выбор_еда_сон = forms.ChoiceField(choices=VS, required=False)  # Выбор
#     Выбор_день_ночь = forms.ChoiceField(choices=DN, required=False) # Выбор
#     Любимый_фильм = forms.CharField(max_length=250)  # Любимый фильм
#
#
#     class Meta:
#         model = models.CustomUser
#         fields = (
#             "username",
#             "email",
#             "password1",
#             "password2",
#             "user_type",
#             "Имя",
#             "Фамилия",
#             "Отчество",
#             "Фото",
#             "Номер_Телефона",
#             "Семейное_Положение",
#             "Пол",
#             "Дети",
#             "Возрост",
#             "Национальность",
#             "Религия",
#             "Страна",
#             "Город",
#             "Адресс",
#             "Хобби",
#             "Языки",
#             "Оброзование",
#             "Профессия",
#             "about_me",
#             "Выбор_еда_сон",
#             "Выбор_день_ночь",
#             "Любимый_фильм",
#         )
#
#     def save(self, commit=True):
#         user = super(CustomUserRegistrations, self).save(commit=False)
#         user.email = self.cleaned_data['email']
#         if commit:
#             user.save()
#         return user
#
#
