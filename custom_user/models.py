from django.db import models
from django.contrib.auth.models import User


class CustomUser(User):

    USER_TYPE = (
        ('Админ', 'Админ'),
        ('Супер_Админ', 'Супер_Админ'),
        ('Клиент', 'Клиент'),
        ('VIP-Клиент', 'VIP-Клиент'),
        ('ЧУВАК', 'ЧУВАК')
    )
    MARIED = (
        ('Да', 'Да'),
        ('Нет', 'Нет')
    )
    GENDER = (
        ('Мужской', 'Мужской'),
        ('Женский', 'Женский')
    )
    CHILDREN = (
        ('Да', 'Да'),
        ('Нет', 'Нет')
    )
    VS = (
        ('СОН', 'СОН'),
        ('ЕДА', 'ЕДА')
    )

    DN = (
        ('ДЕНЬ', 'ДЕНЬ'),
        ('НОЧЬ', 'НОЧЬ')
    )
    user_type = models.CharField(max_length=100, choices=USER_TYPE)
    name = models.CharField(max_length=100) #Имя
    surname = models.CharField(max_length=100)  #Фамилия
    patronymic = models.CharField(max_length=100)  # Отчество
    image = models.ImageField(upload_to='media') #Фото
    phone_number = models.CharField(max_length=13) #Номер
    maried = models.CharField(max_length=10, choices=MARIED) #Семейное положение
    gender = models.CharField(max_length=10, choices=GENDER) #Пол
    children = models.CharField(max_length=10, choices=CHILDREN) #Дети
    age = models.PositiveIntegerField(default=8) #Возрост
    nation = models.CharField(max_length=101)  #Национальность
    weight = models.CharField(max_length=55) # Религия
    country = models.CharField(max_length=50) #Страна
    city = models.CharField(max_length=50) #Город
    address = models.CharField(max_length=100) #Адресс
    hobby = models.CharField(max_length=777) #Хобби
    language = models.CharField(max_length=150) #Языки
    education = models.CharField(max_length=100) #Оброзование
    profession = models.CharField(max_length=100) #Профессия
    about_me = models.TextField()  # О себе
    favorite_vs = models.CharField(max_length=10, choices=VS) #Выбор
    favorite_dn = models.CharField(max_length=10, choices=DN)  # Выбор
    favorite_film = models.CharField(max_length=250, null=True) #Любимый фильм

    def __str__(self):
        return self.user_type