from django import forms
from . import models


class TvshowForm(forms.ModelForm):
    class Meta:
        model = models.Reviews
        fields = "__all__"  # для отдельного поля "title name и так далее"

class FilmViewForm(forms.ModelForm):
    class Meta:
        model = models.Reviews
        fields = '__all__'
