from django.db import models


class Tvshow(models.Model):
    TYPE_TVSHOW = (
        ('Мелодрама', 'Мелодрама'),
        ('Фантастика', 'Фантастика'),
        ('Мультфильмы', 'Мультфильмы'),
        ('Боевик', 'Боевик'),
        ('Комедия', 'Комедия'),
        ('Детектив', 'Детектив'),
        ('Драма', 'Драма')
    )
    title = models.CharField('Название', max_length=100)
    image = models.ImageField('Фото', upload_to='tvshow/', null=True)
    country = models.CharField('Страна', max_length=100)
    atelier = models.CharField('Студия', max_length=100)
    time = models.TimeField('Время')
    release_date = models.DateField('Дата релиза')
    genre = models.CharField('Жанр', max_length=100, choices=TYPE_TVSHOW)
    director = models.TextField('Режиссер')
    starring = models.TextField('В главных ролях')
    description = models.TextField('Описание')
    url_film = models.URLField('Ссылка на фильм')
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self):
        return self.title


class Rating(models.Model):
    RATING_CHOICES = [(str(i), str(i)) for i in range(1, 11)]
    film_rating = models.ForeignKey(Tvshow, on_delete=models.CASCADE, related_name='ratings')
    rating_value = models.CharField('Оценка', max_length=2, choices=RATING_CHOICES, null=True)
    created_at = models.DateTimeField('Дата создания', auto_now_add=True, null=True)

    def __str__(self):
        return self.rating_value


class Reviews(models.Model):
    stars = [(i, '*' * i) for i in range(1, 11)]
    comment = models.CharField('Комментарий', max_length=777, null=True)
    choice_film = models.ForeignKey(Tvshow, on_delete=models.CASCADE, related_name='reviews', null=True)
    rate = models.IntegerField('Оценка', choices=stars, null=True)
    created_date = models.DateField('Дата создания', auto_now_add=True, null=True)

    def __str__(self):
        return self.comment